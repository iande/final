"""Methods for storing sound, for playing it"""

import config
import sounddevice
import soundfile

def play(sound):
    sounddevice.play(sound, config.samples_second)
    sounddevice.wait()


def _mean(sound):
    total = 0
    for sample in sound:
        total += abs(sample)
    return int(total / len(sound))


def draw(sound, period: float, source):
    samples_period = int(period * config.samples_second)
    for nsample in range(0, len(sound), samples_period):
        chunk = sound[nsample: nsample + samples_period]
        mean_sample = _mean(chunk)
        stars = mean_sample //1000
        if source =='square':
            if chunk[0]>0:
                print('*'* stars)
            else:
                print()
        else:
            print('*'* stars)


def store(sound, path: str):
    soundfile.write( path,sound,config.samples_second)
